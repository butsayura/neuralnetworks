﻿using Library;
using System;
using System.IO;
using System.Linq;

namespace NeuronNetworkHebba
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] rawInput = DataReader.ReadData(Directory.GetCurrentDirectory() + "\\data.txt");
            int[][] data = DataReader.ConvertData(rawInput); 

            var network = new Network(data.Length, data[0].Length);

            int iteration;
            bool isTeached = false;
            for (iteration = 0; iteration < 100 && !isTeached; iteration++)
            {
                network.Teach(data);

                isTeached = true;
                for (int i = 0; i < data.Length; i++)
                    if (network.GetResult(data[i]) != i + 1)
                        isTeached = false;
            }

            Console.WriteLine($"Total iterations: " + iteration);

            int num = 1;
            Console.WriteLine(rawInput[num]);
            Console.WriteLine($"{network.GetResult(data[num])} and has to be {num + 1}");

            num = 3;
            Console.WriteLine(rawInput[num]);
            Console.WriteLine($"{network.GetResult(data[num])} and has to be {num + 1}");

            num = 0;
            Console.WriteLine(rawInput[num]);
            Console.WriteLine($"{network.GetResult(data[num])} and has to be {num + 1}");

            num = 2;
            Console.WriteLine(rawInput[num]);
            Console.WriteLine($"{network.GetResult(data[num])} and has to be {num + 1}");
        }
    }
}
