﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronNetworkHebba
{
    public class Network
    {
        private readonly Neuron[] _neurons;

        public Network(int neuronCount, int inputDataCount)
        {
            _neurons = new Neuron[neuronCount + 1];

            for (int i = 0; i < _neurons.Length; i++)
                _neurons[i] = new Neuron(inputDataCount);
        }

        public void Teach(int[][] input)
        {
            for (int i = 0; i < input.Length; i++) {
                for (int j = 0; j < input.Length; j++)
                    _neurons[i].Teach(input[j], i == j ? 1 : -1);
            }
        }

        public int GetResult(int[] inputs)
        {
            int result = 0;
            for (int i = 0; i < _neurons.Length; i++)
                result += (i + 1) * _neurons[i].GetResult(inputs);
            return result;
        }
    }
}
