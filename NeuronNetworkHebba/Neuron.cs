﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuronNetworkHebba
{
    public class Neuron
    {
        private readonly int[] _weights;

        public Neuron(int inputDataCount)
        {
            _weights = new int[inputDataCount + 1];
        }

        public void Teach(int[] inputs, int output)
        {
            _weights[0] += output;

            for (int i = 0; i < inputs.Length; i++)
                _weights[i + 1] += inputs[i] * output;
        }

        public int GetResult(int[] inputs)
        {
            int result = _weights[0];
            for (int i = 0; i < inputs.Length; i++)
                result += _weights[i + 1] * inputs[i];

            return result > 0 ? 1 : 0;
        }
    }
}
