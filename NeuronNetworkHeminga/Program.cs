﻿using Library;
using System;
using System.IO;
using System.Linq;

namespace NeuronNetworkHeminga
{
    class Program
    {
        static string GetFullPath(string fileName)
            => Path.Combine(Directory.GetCurrentDirectory(), fileName);

        static void Main(string[] args)
        {
            string[] rawIdeals = DataReader.ReadData(GetFullPath("ideals.txt"));
            int[][] ideals = DataReader.ConvertData(rawIdeals);
            var network = new Network(ideals);

            string[] rawDataToCheck = DataReader.ReadData(GetFullPath("toDetect.txt"));
            int[][] dataToCheck = DataReader.ConvertData(rawDataToCheck);
            for (int i = 0; i < dataToCheck.Length; i++)
            {
                int detectedValue = network.Detect(dataToCheck[i]);

                if (detectedValue >= 0)
                    Draw(rawDataToCheck[i], rawIdeals[detectedValue]);
                else
                    Draw(rawDataToCheck[i]);
                Console.WriteLine($"Image {i} "
                    + (detectedValue < 0 ? "is not detected" : $"detected as {detectedValue} original.")
                );
                Console.WriteLine();
            }
            Console.Read();
        }

        static void Draw(string fromStr, string toStr)
        {
            var from = fromStr.Split("\r\n").Where(_ => !string.IsNullOrWhiteSpace(_)).ToArray();
            var to = toStr.Split("\r\n").Where(_ => !string.IsNullOrWhiteSpace(_)).ToArray();

            for (int i = 0; i < from.Length; i++)
            {
                string s = from[i];
                s += i == from.Length / 2
                    ? "  ->  "
                    : "      ";
                s += to[i];
                Console.WriteLine(s);
            }
        }

        static void Draw(string fromStr)
        {
            var from = fromStr.Split("\r\n").Where(_ => !string.IsNullOrWhiteSpace(_)).ToArray();
            foreach (var line in from)
                Console.WriteLine(line);
        }
    }
}
