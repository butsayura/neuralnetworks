﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronNetworkHeminga
{
    public class Network
    {
        private readonly double[][] _Sw;
        private double[] _Zin, _Zout;

        public Network(int[][] ideals)
        {
            _Zin = new double[ideals.Length];
            _Sw = new double[ideals[0].Length][];
            for (int i = 0; i < _Sw.Length; i++)
                _Sw[i] = new double[ideals.Length];
            for (int i = 0; i < _Sw.Length; i++)
                for (int j = 0; j < ideals.Length; j++)
                    _Sw[i][j] = ideals[j][i] / 2.0;

        }

        private double G(double uIn) => uIn > 0 ? uIn : 0;

        private int statistic = 0;
        private double Ua(int i, int t)
        {
            statistic++;
            if (t == 0)
                return _Zout[i];

            double eps = 1.0 / _Zout.Length;

            double sum = 0;
            for (int j = 0; j < _Zin.Length; j++)
                if (j != i)
                    sum += Ua(j, t - 1);

            return G(Ua(i, t - 1) - eps * sum);
        }

        private double[] CalculateAout()
        {
            double[] a = new double[] { 1, 1 };
            for (int t = 1; a.Count(_ => _ != 0) > 1; t++)
                a = Enumerable.Range(0, _Zout.Length)
                    .Select(_ => Ua(_, t)).ToArray();
            return a;
        }

        public int Detect(int[] data)
        {
            for (int i = 0; i < _Zin.Length; i++)
            {
                _Zin[i] = (double)data.Length / 2;
                for (int j = 0; j < data.Length; j++)
                    _Zin[i] += _Sw[j][i] * data[j];
            }

            double max = _Zin.Max();
            if (_Zin.Count(_ => _ == max) > 1)
                return -1;

            for (int i = 0; i < _Zin.Length; i++)
                if (_Zin[i] == max)
                    return i;      

            _Zout = _Zin.Select(_ => _ <= 0 ? 0 : _).ToArray();

            var aOut = CalculateAout();
            for (int i = 0; i < aOut.Length; i++)
                if (aOut[i] > 0)
                    return i;

            return -1;
        }
    }
}
