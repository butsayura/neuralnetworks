﻿using Library;
using System;
using System.IO;
using System.Linq;

namespace NeuronNetworkRozenblantta
{
    class Program
    {
        static string GetFullPath(string fileName)
            => Path.Combine(Directory.GetCurrentDirectory(), fileName);

        static void Main(string[] args)
        {
            string[] rawIdeals = DataReader.ReadData(GetFullPath("ideals.txt"));
            int[][] ideals = DataReader.ConvertData2(rawIdeals);
            var network = new Network(ideals, 6);

            string[] rawDataToCheck = DataReader.ReadData(GetFullPath("toDetect.txt"));
            int[][] dataToCheck = DataReader.ConvertData2(rawDataToCheck);

            for (int i = 0; i < dataToCheck.Length; i++)
            {
                int detectedValue = network.Detect(dataToCheck[i]);

                Console.WriteLine($"Image {i} == {detectedValue}\n");
            }
            Console.Read();
        }
    }
}
