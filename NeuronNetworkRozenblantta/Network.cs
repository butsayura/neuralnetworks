﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronNetworkRozenblantta
{
    public class Network
    {
        private double[][] _Sw;
        private double[] _Aw;
        private double _Sp;
        private double _Rp;

        public Network(int[][] ideals, int count)
        {
            var rand = new Random();
            _Sw = new double[count][];
            for (int i = 0; i < _Sw.Length; i++)
            {
                _Sw[i] = new double[ideals[0].Length];
                for (int j = 0; j < _Sw[i].Length; j++)
                    _Sw[i][j] = (rand.Next(9) + 1) / 10.0;
            }
            _Sw = new double[][]
            {
                new double[] {0.3, 0.2, 0.1, 0.6, 0.5, 0.4, 0.9, 0.6, 0.7 },
                new double[] {0.2, 0.1, 0.3, 0.4, 0.5, 0.6, 0.7, 0.1, 0.9 },
                new double[] {0.3, 0.5, 0.1, 0.6, 0.5, 0.4, 0.9, 0.4, 0.7 },
                new double[] {0.4, 0.3, 0.2, 0.1, 0.8, 0.7, 0.6, 0.6, 0.9 },
                new double[] {0.5, 0.3, 0.3, 0.6, 0.1, 0.2, 0.9, 0.2, 0.7 },
                new double[] {0.6, 0.5, 0.4, 0.1, 0.2, 0.3, 0.8, 0.5, 0.8 },
            };

            _Aw = new double[count];
            for (int i = 0; i < _Aw.Length; i++)
                _Aw[i] = (rand.Next(9) + 1) / 10.0;
            _Aw = new double[] { 0.2, 0.8, 0.6, 0.9, 0.8, 0.1 };

            Teach(ideals);
        }

        private void Teach(int[][] ideals)
        {
            var aIn = ideals
                .Select(data => CalculateAin(data))
                .ToArray();
            //aIn[1] = new double[] { 3.2, 3.2, 3.4, 3.2, 3.6, 3.5 };

            _Sp = CalculateSp(aIn);

            var aOut = aIn.Select(data => CalculateAout(data)).ToArray();

            _Rp = aOut.Select(a => CalculateRin(a)).Average();
            var testingData = GetIndexes(aOut).ToList();

            int currentTestDataIndex = 0;
            while (testingData.Any())
            {
                var indexes = testingData[currentTestDataIndex];

                if (FixWeight(indexes))
                {
                    currentTestDataIndex--;
                    testingData.Remove(indexes);
                }

                currentTestDataIndex++;
                if (currentTestDataIndex >= testingData.Count)
                    currentTestDataIndex = 0;
            }
        }

        private int Normalize(double n) => n >= _Sp ? 1 : 0;

        private double CalculateSp(double[][] aIn)
        {
            var numbers = aIn
                .SelectMany(_ => _)
                .Distinct()
                .OrderBy(_ => _)
                .ToArray();

            int score = 0;
            double sp = 0;
            foreach (var t in numbers)
            {
                int currentScore = 0;
                for (int i = 0; i < aIn[0].Length; i++)
                    if (aIn[0][i] >= t ^ aIn[1][i] >= t)
                        currentScore++;
                if (currentScore > score)
                {
                    score = currentScore;
                    sp = t;
                }
            }
            return sp;
        }

        private IEnumerable<int[]> GetIndexes(int[][] data)
        {
            return data
                .Select(d =>
                {
                    var result = new List<int>();
                    for (int i = 0; i < d.Length; i++)
                        if (d[i] > 0)
                            result.Add(i);
                    return result.ToArray();
                });
        }

        private double[] CalculateAin(int[] data)
        {
            return _Sw.Select(sw =>
            {
                double result = 0;
                for (int i = 0; i < sw.Length; i++)
                    result += sw[i] * data[i];
                return result;
            })
            .ToArray();
        }

        private int[] CalculateAout(double[] aIn)
            => aIn.Select(_ => Normalize(_)).ToArray();

        private double CalculateRin(int[] aOut)
        {
            double result = 0;
            for (int i = 0; i < aOut.Count(); i++)
                result += aOut[i] * _Aw[i];
            return result;
        }

        private double CalculateRinByIndexes(int[] a)
        {
            double result = 0;
            for (int i = 0; i < a.Count(); i++)
                result += _Aw[a[i]];
            return result;
        }

        private int CalculateRout(double rIn) => rIn > _Rp ? 1 : -1;

        private bool FixWeight(int[] indexes)
        {
            const double eta = 0.1;
            var rIn = CalculateRinByIndexes(indexes);

            if (rIn == _Rp)
                return true;

            foreach (var index in indexes)
                if (rIn > _Rp)
                {
                    if (_Aw[index] > 0)
                        _Aw[index] -= eta;
                }
                else if (_Aw[index] < 1)
                    _Aw[index] += eta;

            var new_rIn = CalculateRinByIndexes(indexes);

            if (new_rIn == _Rp)
                return true;

            return rIn > _Rp ^ new_rIn > _Rp;
        }

        public int Detect(int[] data)
        {
            var aIn = CalculateAin(data);
            var aOut = CalculateAout(aIn);
            double rIn = CalculateRin(aOut);
            int rOut = CalculateRout(rIn);
            return rOut;
        }
    }
}
