﻿using Library;
using System;
using System.IO;
using System.Linq;

namespace NeuronNetworkHopfilda
{
    class Program
    {
        static string GetFullPath(string fileName)
            => Path.Combine(Directory.GetCurrentDirectory(), fileName);

        static void Main(string[] args)
        {
            Matrix.Width = 6;

            int[][] ideals = DataReader.ReadNormalizedData(GetFullPath("ideals.txt"));
            var network = new Network(ideals);

            int[][] dataToCheck = DataReader.ReadNormalizedData(GetFullPath("toDetect.txt"));
            for (int i  = 0; i < dataToCheck.Length; i++)
            {
                int detectedValue = network.Detect(dataToCheck[i]);
                Console.WriteLine($"Image {i + 1} "
                    + (detectedValue < 0 ? "is not detected" : $"detected as {detectedValue} original.") 
                );
                Console.WriteLine();
            }
            Console.Read();
        }
    }
}
