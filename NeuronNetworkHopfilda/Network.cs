﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NeuronNetworkHopfilda
{
    public class Network
    {
        private const int MaxIter = 100;
        private readonly Matrix _matrix;
        private readonly Matrix[] _ideals;
        private readonly int _p;


        public Network(int[][] ideals)
        {
            _ideals = ideals.Select(_ => new Matrix(_, Align.Vertical)).ToArray();
            _matrix = new Matrix(ideals[0].Length, ideals[0].Length);
            foreach (var ideal in ideals)
                _matrix += new Matrix(ideal, Align.Vertical) * new Matrix(ideal, Align.Horizontal);

            _matrix.ClearDiagonal();
            _p = _matrix.GetP();
        } 

        public int Detect(int[] data)
        {
            int result = -1;
            var before = new Matrix(data, Align.Vertical);

            int p = 0;
            Matrix y = null;
            while(result < 0 && Math.Abs(p) <= _p)
            {
                y = new Matrix(data, Align.Vertical);
                for (int iter = 0; iter < MaxIter && result < 0; iter++)
                {
                    var prev = y.Clone();
                    y = _matrix * y;
                    y.Normalize();
                    result = Check(y);

                    if (prev.Equals(y))
                        break;
                }
                p = p > 0 ? -p : Math.Abs(p) + 1;
            }

            if (result >= 0)
                Matrix.DebugTool(before, y);
            return result;
        }

        private int Check(Matrix y)
        {
            for (int i = 0; i < _ideals.Length; i++)
                if (_ideals[i].Equals(y))
                    return i;
            return -1;
        }
    }
}
