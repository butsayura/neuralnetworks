﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace NeuronNetworkHopfilda
{
    public class Matrix
    {
        private int[][] _data;

        public Matrix(int lines, int columns)
        {
            _data = new int[lines][];
            for (int i = 0; i < lines; i++)
                _data[i] = new int[columns];
        }

        public Matrix(Matrix matrix)
        {
            _data = (int[][])matrix._data.Clone();
        }

        public Matrix Clone()
        {
            return new Matrix(this);
        }

        public Matrix(int[][] data)
        {
            _data = (int[][])data.Clone();
        }

        public Matrix(int[] data, Align align)
        {
            if (align == Align.Vertical)
            {
                _data = new int[data.Length][];
                for (int i = 0; i < data.Length; i++)
                    _data[i] = new int[] { data[i] };
            }
            else
                _data = new int[][] { (int[])data.Clone() };
        }

        public void ClearDiagonal()
        {
            for (int i = 0; i < _data.Length; i++)
                _data[i][i] = 0;
        }

        public static Matrix operator *(Matrix a, Matrix b)
        {
            var matrix = new Matrix(a._data.Length, b._data[0].Length);

            for (int i = 0; i < a._data.Length; i++)
                for (int j = 0; j < b._data[0].Length; j++)
                    for (int k = 0; k < a._data[i].Length; k++)
                        matrix._data[i][j] += a._data[i][k] * b._data[k][j];

            return matrix;
        }

        public static Matrix operator +(Matrix a, Matrix b)
        {
            var matrix = new Matrix(a);
            for (int i = 0; i < matrix._data.Length; i++)
                for (int j = 0; j < matrix._data[i].Length; j++)
                    matrix._data[i][j] += b._data[i][j];
            return matrix;
        }

        public void Normalize(int p = 0)
        {
            for (int i = 0; i < _data.Length; i++)
                for (int j = 0; j < _data[i].Length; j++)
                    _data[i][j] = _data[i][j] > p ? 1 : -1;
        }

        public override string ToString()
        {
            return _data.Select(line =>
                    line
                        .Select(_ => _.ToString())
                        .Aggregate((current, next) => current + ", " + next)
                )
                .Aggregate((current, next) => current + "\n" + next);
        }

        public override bool Equals(object obj)
        {
            if (obj is Matrix matr)
                return Equals(matr);
            return false;
        }

        public bool Equals(Matrix matrix)
        {
            if (matrix._data.Length != _data.Length || matrix._data[0].Length != _data[0].Length)
                return false;

            for (int i = 0; i < _data.Length; i++)
                for (int j = 0; j < _data[i].Length; j++)
                    if (_data[i][j] != matrix._data[i][j])
                        return false;
            return true;
        }

        public int GetP()
        {
            return Math.Abs(_data.Sum(_ => _.Sum(i => i)) / 2);
        }

        public static int Width = 6;
        public static void DebugTool(Matrix before, Matrix after)
        {
            var pos = 0;
            int iter = 0;

            while (pos < before._data.Length)
            {
                for (int i = 0; i < Width; i++)
                    Console.Write(before._data[i + pos].First() == 1 ? "*" : "-");

                if (iter++ == before._data.Length / Width / 2)
                    Console.Write("  ->  ");
                else
                    Console.Write("      ");

                for (int i = 0; i < Width; i++)
                    Console.Write(after._data[i + pos].First() == 1 ? "*" : "-");
                pos += Width;

                Console.WriteLine();
            }
        }
    }

    public enum Align
    {
        Vertical,
        Horizontal,
    }
}
