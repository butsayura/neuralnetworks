﻿using System;
using System.IO;
using System.Linq;

namespace Library
{
    public class DataReader
    {
        public static string[] ReadData(string fileName)
        {
            using (var streamReader = new StreamReader(fileName))
            {
                string str = streamReader.ReadToEnd();
                return str.Split(';').ToArray();
            }
        }

        public static int[][] ConvertData(string[] rawData)
        {
            return rawData
                .Select(_ => _
                    .Where(c => "*-".Contains(c))
                    .Select(c => c == '*' ? 1 : -1).ToArray())
                .ToArray();
        }

        public static int[][] ConvertData2(string[] rawData)
        {
            return rawData
                .Select(_ => _
                    .Where(c => "*-".Contains(c))
                    .Select(c => c == '*' ? 1 : 0).ToArray())
                .ToArray();
        }

        public static int[][] ReadNormalizedData(string fileName)
            => ConvertData(ReadData(fileName));
    }
}
